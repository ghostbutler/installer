#!/bin/bash

for service in "scanner" "directory" "hue" "mfi" "securitymanager" "event" "sensor" "iconprovider" "tts" "orchestrator"
do
	gnome-terminal --title "$service" -x sh -c "tail -f /var/log/syslog | grep --line-buffered \"ghost2 $service\""
done

