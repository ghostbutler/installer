#!/bin/bash

for service in "event" "texttospeech" "hue" "mfi" "directory" "securitymanager" "sensor" "scanner" "ghostgamepad" "iconprovider" "status"
do
	sudo systemctl stop $service
	sudo systemctl disable $service
done

rm -rf cloner device ghostbutler instrumentation service tool clone.go
