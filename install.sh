#!/bin/bash

export PATH=$PATH:/usr/lib/go-1.11/bin

ROOT=`pwd`

string_length( ) {
	length=`echo -n "$1" | wc -c`
	return $length
}

color="\033[1;31m"

write_title( ) {
	string_length "$1"
	for i in `seq 1 $(($?+4))`; do echo -en "$color*"; done
	echo ""
	echo -e "$color* $1 *\033[0m"
	string_length "$1"
	for i in `seq 1 $(($?+4))`; do echo -en "$color*\033[0m"; done
        echo -e "\033[0m"
}

talk_remote ( ) {
	curl -d "{\"sentence\":\"$1\", \"language\": \"fr\", \"volume\": 100}" -k https://192.168.0.119:16570/api/v1/controller/texttospeech -X POST
}

talk_local( ) {
        curl -d "{\"sentence\":\"$1\", \"language\": \"fr\", \"volume\": 100}" -H "X-Ghost-Token: `accessgenerator 127.0.0.1 GhostButler2018Scanner "GhostButlerProject2018*"`" -k https://127.0.0.1:16570/api/v1/controller/texttospeech -X POST
}

# clone repo
talk_remote "Je récupère le cloneur"
write_title "Recupération du cloner"
git clone https://gitlab.com/ghostbutler/cloner.git
mv cloner/clone.go .
sleep 2

# clone whole project tree
talk_remote "Je clone tout le projet"
write_title "Clonage du projet"
go run clone.go
mv ghostbutler/* .

# install services
talk_remote "J'installe tous les services"
write_title "Installation des services..."
set -- "service/backend/securitymanager" "service security manageur" "service/backend/scanner" "service de scan" "service/backend/event" "service événement" "service/frontend/iconprovider" "fournisseur d'icône" "service/backend/directory" "service directory" "device/controller/hue" "contrôleur de lumières" "device/controller/mfi" "contrôleur de prises" "device/controller/ghostgamepad" "contrôleur de manettes" "device/controller/ghostmusic" "contrôleur de musique" "device/object/texttospeech" "service de synthèse vocale" "service/backend/orchestrator" "service orchestrateur" "service/backend/sensor" "service sensor" "service/backend/status" "service status"
while [ "$#" -gt 0 ]; do
	cd $1
	talk_remote "J'installe le $2"
	write_title "Installation du $2"
	sudo ./install.sh
	if [ "$1" == "device/object/texttospeech" ]
	then
		sleep 2
		talk_local "Je viens d'être installé"
	fi
	read  -n 1 -p "" osef
	cd $ROOT
	shift 2
done

# cleanup
rm -rf cloner/
rm clone.go

